module invent.kde.org/sysadmin/geoip-service-backend.git

go 1.14

require (
	github.com/bytedance/sonic v1.9.2 // indirect
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/getsentry/sentry-go v0.22.0
	github.com/gin-gonic/gin v1.9.1
	github.com/go-playground/validator/v10 v10.14.1 // indirect
	github.com/klauspost/cpuid/v2 v2.2.5 // indirect
	github.com/oschwald/geoip2-golang v1.9.0
	github.com/pelletier/go-toml/v2 v2.0.9 // indirect
	github.com/stretchr/testify v1.8.4
	golang.org/x/arch v0.4.0 // indirect
	golang.org/x/net v0.12.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
