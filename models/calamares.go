// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package models

// CalamaresGeoIP is able to serialize into calamares' JSON
type CalamaresGeoIP struct {
	TimeZone string `json:"time_zone"`
}

// This bugger has no New method because it's literally one member. Should
// this change in the future a New meth should be introduced.
