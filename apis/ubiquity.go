// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package apis

import (
	"net/http"

	"github.com/gin-gonic/gin"
	geoip2 "github.com/oschwald/geoip2-golang"
	"invent.kde.org/sysadmin/geoip-service-backend.git/models"
)

// We are muddying the waters a bit by merging api+service+data.
type ubiquityResource struct {
	db *geoip2.Reader
}

// ServeUbiquityResource sets up the ubiquity resource routes.
func ServeUbiquityResource(rg *gin.RouterGroup, db *geoip2.Reader) {
	r := &ubiquityResource{db}
	rg.GET("/v1/ubiquity", r.get)
}

/**
 * @api {get} /ubiquity Ubiquity
 *
 * @apiVersion 1.0.0
 * @apiGroup GeoIP
 * @apiName ubiquity
 *
 * @apiDescription Ubuiqity-style XML geoip data. This is equivalent to calling
 *    geoip.ubuntu.com/lookup which is where the actual data format comes from.
 *
 * @apiSuccessExample {xml} Success-Response:
 *   <Response>
 *   <script/>
 *   <Ip>193.81.57.56</Ip>
 *   <Status>OK</Status>
 *   <CountryCode>AT</CountryCode>
 *   <CountryCode3/>
 *   <CountryName>Austria</CountryName>
 *   <RegionCode>4</RegionCode>
 *   <RegionName>Upper Austria</RegionName>
 *   <City>Gmunden</City>
 *   <ZipPostalCode>4810</ZipPostalCode>
 *   <Latitude>47.9022</Latitude>
 *   <Longitude>13.7642</Longitude>
 *   <AreaCode>0</AreaCode>
 *   <TimeZone>Europe/Vienna</TimeZone>
 *   </Response>
 */
func (r *ubiquityResource) get(c *gin.Context) {
	// If you are using strings that may be invalid, check that ip is not nil
	ip := clientIP(c)
	record, err := r.db.City(ip)
	if err != nil {
		panic(err)
	}

	data := models.NewUbiquityGeoIPFromGeoIP2Record(ip.String(), record)
	c.XML(http.StatusOK, data)
}
