// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package apis

import (
	"net/http"
	"testing"

	geoip2 "github.com/oschwald/geoip2-golang"
)

func TestCalamaresResource(t *testing.T) {
	db, err := geoip2.Open("../GeoLite2-City.mmdb")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	ServeCalamaresResource(router.Group("/"), db)

	kdeDotOrg := `{"time_zone":"Europe/London"}`
	runAPITests(t, []apiTestCase{
		{"t1 - get", "GET", "/v1/calamares", "", http.StatusOK, kdeDotOrg, equalJSON},
		{"t2 - get timezone", "GET", "/v1/timezone", "", http.StatusOK, kdeDotOrg, equalJSON},
	})
}
