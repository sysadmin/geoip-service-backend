// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package apis

import (
	"net"

	"github.com/gin-gonic/gin"
)

func clientIP(c *gin.Context) net.IP {
	if ip := c.Query("ip"); len(ip) > 0 {
		return net.ParseIP(ip)
	} else if ip := c.ClientIP(); len(ip) > 0 {
		return net.ParseIP(ip)
	}
	panic("Couldn't resolve client IP")
}
