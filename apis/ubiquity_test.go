// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package apis

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	geoip2 "github.com/oschwald/geoip2-golang"
	"github.com/stretchr/testify/assert"
	"invent.kde.org/sysadmin/geoip-service-backend.git/models"
)

func equalUbiquity(t *testing.T, test apiTestCase, res *httptest.ResponseRecorder) {
	assert.Contains(t, res.Header().Get("Content-Type"), "application/xml", test.tag)
	var expectedObj, actualObj models.UbiquityGeoIP
	if err := xml.Unmarshal([]byte(test.response), &expectedObj); err != nil {
		assert.FailNow(t, fmt.Sprintf("Input ('%s') needs to be valid xml.\nXML parsing error: '%s'", test.response, err.Error()), test.tag)
	}
	if err := xml.Unmarshal(res.Body.Bytes(), &actualObj); err != nil {
		assert.FailNow(t, fmt.Sprintf("Expected value ('%s') is not valid xml.\nXML parsing error: '%s'", res.Body.String(), err.Error()), test.tag)
	}
	assert.Equal(t, expectedObj, actualObj, test.tag)
}

func TestUbiquityResource(t *testing.T) {
	db, err := geoip2.Open("../GeoLite2-City.mmdb")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	ServeUbiquityResource(router.Group("/"), db)

	kdeDotOrg := `
<Response>
<script/>
<Ip>91.189.93.5</Ip>
<Status>OK</Status>
<CountryCode>GB</CountryCode>
<CountryCode3/>
<CountryName>United Kingdom</CountryName>
<RegionCode>ENG</RegionCode>
<RegionName>England</RegionName>
<City>London</City>
<ZipPostalCode>EC2V</ZipPostalCode>
<Latitude>51.5095</Latitude>
<Longitude>-0.0955</Longitude>
<AreaCode>0</AreaCode>
<TimeZone>Europe/London</TimeZone>
</Response>`
	runAPITests(t, []apiTestCase{
		{"t1 - get", "GET", "/v1/ubiquity", "", http.StatusOK, kdeDotOrg, equalUbiquity},
	})
}
