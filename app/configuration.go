// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018-2020 Harald Sitter <sitter@kde.org>

package app

import (
	"io/ioutil"
	"os"
	"path/filepath"

	yaml "gopkg.in/yaml.v2"
)

// Conf is the global configuration object of the app configuration.
var Conf = loadConfiguration()

// Config encapsulates the deserialized app configuration.
type Config struct {
	// maxmind license key
	// https://blog.maxmind.com/2019/12/18/significant-changes-to-accessing-and-using-geolite2-databases/
	LicenseKey string `yaml:"license_key"`
}

func loadConfiguration() *Config {
	path := filepath.Join(os.Getenv("HOME"), ".config/geoip-kde-org.yaml")
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		panic("missing ~/.config/geoip-kde-org.yaml")
	}

	y := &Config{}
	err = yaml.Unmarshal(yamlFile, y)
	if err != nil {
		panic(err)
	}

	return y
}
