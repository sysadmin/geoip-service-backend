<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2018-2020 Harald Sitter <sitter@kde.org>
-->

# Description

GeoIP service based on Maxmind's GeoLite2 data. Supports multiple API endpoints
for different output formats for different installers. The underlying GeoLite2
data is automatically updated on every start iff the existing data were last
updated more than a week ago. The service automatically terminates after 14 days
of uptime (thus triggering the aforementioned update). Because of this the
listening sockets are managed through systemd so no connections are lost during
this restart dance.

# Requirements

Needs Go 1.8

# Deployment

Database it automatically downloaded and managed in PWD, so PWD should be suitable.
systemd/* contains example socket and service.

# License Key

To actually work, the service requires a license_key to be set in ~/.config/geoip-kde-org.yaml.
The production key is managed by KDE sysadmins. For testing it's probably best to make your
own account and get your own key.

Running without a license key is simply not possible

- https://blog.maxmind.com/2019/12/18/significant-changes-to-accessing-and-using-geolite2-databases/
- https://dev.maxmind.com/geoip/geoipupdate/

# Documentation

Documentation uses apidocjs.com. Run `make doc` to generate it (requires npm).

# Try it

You may wanna read the most basic documention on how to use the `go` tool to
get/build/install stuff.

TLDR:

```
export GOPATH=$WHEREVER; go install anongit.kde.org/sysadmin/geoip-service-backend.git && $GOPATH/bin/geoip-service-backend.git
```

By default the service listens on localhost:8080, this can be influenced via
environment variables HOST and PORT. It's however suggested to use systemd for
socket management as it allows 0-downtime restarts of the service
(i.e. without any connections not getting answered).

Note that the makefile is largely meant for use within KDE and isn't meant
to be run outside our services at this time. If you read the makefile and find
this applicable for yourself you can of course use it though!
